<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" href="http://rjavier.com.mx/wordpress/favicon.ico">
	
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/articles.css">
	<title><?php echo the_title();?></title>
</head>
<body>
	<section id="full-Content">
		<!-- This section it for import the header and Nav-->
		<section id="header">
			<?php get_header(); ?>
			
		</section>
		<!-- This section it for import the content frontpage or entries if you use a sidebar  upload into this -->
		<section id="content">	
			
		</section>
		
		<!-- This section it for import the footer-->
		<section id="footer">
			<?php get_footer(); ?>
		</section>
	</section>
</body>
</html>