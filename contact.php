<?php
/*
Template Name: contact
*/
?>
<?php
/*
 * Template Name: Mi pagina contact
 * Description: Plantilla de pagina personalizado 1.
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" href="http://rjavier.com.mx/wordpress/favicon.png">
	<link href="<?php echo get_template_directory_uri();?>/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/stylecontact.css">

	<title><?php echo the_title();?></title>
</head>
<body>
	<section id="full-Content">
		<!-- This section it for import the header and Nav-->
		<header id="header">
			<?php get_header(); ?>
			
		</header>
		<!-- This section it for import the content frontpage or entries if you use a sidebar  upload into this -->
		<section id="content">
				<div id="map" >
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						 <div class="entry">
		  					 <?php the_content(); ?>
		 				</div>
						<?php endwhile; else: ?>
						<p><?php _e('Sorry, no found content.'); ?></p>
						<?php endif; ?>
				</div>
				<section id="contactUS">
					<div id="contactUS-content">
						<div>
							<form>
								<h2>Contact</h2>
								<input type="text" placeholder="Name" required />
								<input type="email" placeholder="Email" required/>
								<textarea placeholder="Message"></textarea>
								<button >Send</button>
							</form>
						</div>
						<div id="follow">
							<h2>Follow</h2>
							<ul id="followLi">
								<li><a href = "https://www.facebook.com"><i class="fa fa-facebook fa-2x"></i></a></li>
								<li><a href = "https://www.twitter.com"><i class="fa fa-twitter fa-2x"></i></a></li>
								<li><a href = "https://plus.google.com"><i class="fa fa-google-plus fa-2x"></i></a></li>
								<li><a href = "https://mx.linkedin.com"><i class="fa fa-linkedin-square fa-2x"></i></a></li>
								
							</ul>		
						</div>
					</div>
				</section>
		</section>
		
		<!-- This section it for import the footer-->
		<footer id="footer">
			<?php get_footer(); ?>
		</footer>
	</section>
	
	<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/functions.js"></script>    
</body>
</html>
<!--code for wordpress

<ul id="contactInfo">
	<li id="email"><i class="fa fa-envelope"> </i> info@bluetrailsoft.com</li>
	<li id="street"><i class="fa fa-map-marker"> </i> Niños Heroes 115</li>
	<li id="col"><i class="glyphiconNull"> </i> Col. centro</li>
	<li id="CP"><i class="glyphiconNull"></i> 47600 Tepatitlan de Morelos</li>
	<li id="state"><i class="glyphiconNull"></i> Jalisco</li>
	<li id="phoneMx"><i class="fa fa-phone"> </i> Mx: 378 715 5420</li>
	<li id="phoneUs"><i class="glyphiconNull"></i>  US: 415 240 4550</li>
</ul>
-->
