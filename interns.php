<?php 
	//Template Name: interns
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" href="http://rjavier.com.mx/wordpress/favicon.ico">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/interns.css">
	<title><?php echo the_title();?></title>
</head>
<body>
	<section id="full-Content">
		<!-- This section it for import the header and Nav-->
		<section id="header">
			<?php get_header(); ?>
			
		</section>

		<!-- This section it for import the content frontpage or entries if you use a sidebar  upload into this -->
		<section id="content">
		<article id="description">
					<?php 
					query_posts('category_name=description');
					if ( have_posts() ) : while ( have_posts() ) : the_post(); 
					?>
					<article class="descrip">
						<?php the_content(); ?>
						<p class="des-title"><?php the_title(); ?></p>
					</article>
					<?php
					endwhile; else:
					?>
					<p>
					<?php 
					_e('Sorry, no posts matched your criteria.'); 
					?>
					</p>
					<?php endif; ?>
					<div style="clear:both;"></div>

		</article>	
		<article id="title">
			<h2>Our Interns</h2>
		</article>	
			
				<div id="center">
					<div id="content-interns">
						<?php 
						query_posts('category_name=interns');
						if ( have_posts() ) : while ( have_posts() ) : the_post(); 
						?>
						<article class="profile">
							
							<?php the_content(); ?>
							<p class="name"><?php the_title(); ?></p>

						</article>
						<?php
						endwhile; else:
						?>
						<p>
						<?php 
						_e('Sorry, no posts matched your criteria.'); 
						?>
						</p>
						<?php endif; ?>
					<div style="clear:both;"></div>
				</div>		
				</div>
			
		</section>
		
		<!-- This section it for import the footer-->
		<section id="footer">
			<?php get_footer(); ?>
		</section>
	</section>
	
	<script src="<?php echo get_template_directory_uri();?>/js/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/functions.js"></script>    
</body>
</html>