
<section id = "total">
	<section id = "one">
		<div id = "one1">
			<h4>Get in touch with us!</h4>
			<pre>email:	info@bluetrailsoft.com</pre>
			<pre>call:	MX: 378 731 8188</pre>
			<pre>		US: 415 247 9504</pre>
			<pre>or</pre>
			<pre>visit:	Niños Heroes 115</pre>
			<pre>		Tepreatitlán Jalisco 47600</pre>
		</div>
	</section>

	<section id = "two">
		<a href = "https://www.google.com.mx/maps/place/20%C2%B048'49.9%22N+102%C2%B045'52.7%22W/@20.8136534,-102.7639093,17z/data=!4m2!3m1!1s0x0:0x0">
			<img src="<?php echo get_template_directory_uri();?>/images/map.png"></a>
	</section>

	<section id="three">
		<h4>Follow us</h4>
		<div id="img1">
			<a href = "https://www.facebook.com"> <img  src="<?php echo get_template_directory_uri();?>/images/Facebook.png"> </a>
			<a href = "https://www.twitter.com"><img  src="<?php echo get_template_directory_uri();?>/images/Twitter.png">	</a>
		</div>
		<div id="img2">
			<a href = "https://mx.linkedin.com"><img  src="<?php echo get_template_directory_uri();?>/images/LinkedIn.png"></a>
			<a href = "https://plus.google.com"><img  src="<?php echo get_template_directory_uri();?>/images/Google.png"></a>
		</div>-
	</section>

	<section id = "four">
		<h4> Site Map </h4>	
		<?php wp_page_menu();?>
		
	</section>

	<section id = "five">
		<h4>Blue Trail Software</h4>
		<p>Copyright &copy; <?php echo date('Y') ?>. Blue Trail Software S.A. de C.V. </p>
		<p>All Rights Reserved</p>
	</section>
</section>
<?php wp_footer(); ?>
