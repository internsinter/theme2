<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	
	<link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" href="http://rjavier.com.mx/wordpress/favicon.ico">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/articles.css">
	
	<title><?php echo the_title();?></title>
</head>
<body>
	<section id="full-Content">
		<!-- This section it for import the header and Nav-->
		<section id="header">
			<?php get_header(); ?>
			
		</section>
		<!-- This section it for import the content frontpage or entries if you use a sidebar  upload into this -->
		<section id="content">
			<!--<form action="<?php bloginfo('url'); ?>" method="get">
				<?php wp_dropdown_pages();?>
				<input type="submit" name="submit" value="view" />
   			</form>-->
   			<!--<?php $args = array(
   			    'depth'       => 0,
   				'sort_column' => 'menu_order, post_title',
   				'menu_class'  => 'menu',
   				'include'     => '',
   				'exclude'     => '',
   				'echo'        => true,
   				'show_home'   => false,
   				'link_before' => '',
   				'link_after'  => '' );
   				echo "<ul><li>Menu";wp_page_menu( $args ); echo "</li></ul>";
   			?>-->
   			<?php
   			echo "<ul><li>Menu";
   			echo '<div>';
   			echo '<ul>';
				while ( have_posts() ) : the_post();
					echo '<li>';
					echo '<a href=';the_permalink();
					echo '>';
					the_title();
					echo '</a>';
					echo '</li>';
			endwhile;
			echo '</ul>';
			echo '</div>';
			echo "</li></ul>";
				?>
			<?php
			query_posts('category_name=Article');
			if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<div class="entry">
								<?php the_title('<h2>', '</h2>'); ?>
								<?php the_content(); ?>
							</div>
						<?php endwhile; else: ?>
							<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
						<?php endif; ?>
	
		</section>
		
		<!-- This section it for import the footer-->
		<section id="footer">
			<?php get_footer(); ?>
		</section>
	</section>
</body>
</html>
