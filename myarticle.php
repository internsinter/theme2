<?php
	/*
	Template Name: Article
	*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" href="http://rjavier.com.mx/wordpress/favicon.ico">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
	
	<title><?php echo the_title();?></title>
</head>
<body>
	<section id="full-Content">
		<!-- This section it for import the header and Nav-->
		<section id="header">
			<?php get_header(); ?>
			
		</section>
		<!-- This section it for import the content frontpage or entries if you use a sidebar  upload into this -->
		<section id="content">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="entry">
					<?php the_title('<h2>', '</h2>'); ?>
					<?php the_content(); ?>
				</div>
			<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>
		</section>
		
		<!-- This section it for import the footer-->
		<section id="footer">
			<?php get_footer(); ?>
		</section>
	</section>   
</body>
</html>


<!--<?php $args = array(
				'authors'      => '',
				'child_of'     => 0,
				'date_format'  => get_option('date_format'),
				'depth'        => 0,
				'echo'         => 1,
				'exclude'      => '',
				'include'      => '',
				'link_after'   => '',
				'link_before'  => '',
				'post_type'    => 'page',
				'post_status'  => 'publish',
				'show_date'    => '',
				'sort_column'  => 'menu_order, post_title',
			        'sort_order'   => '',
				'title_li'     => __('Pages'), 
				'walker'       => ''
			); 
			wp_list_pages( $args );	?>-->
