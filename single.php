<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<link href="http://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
	<link rel="shortcut icon" href="http://rjavier.com.mx/wordpress/favicon.ico">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/articles.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/single.css">
	
	<title><?php echo the_title();?></title>
</head>
<body>
	<section id="full-Content">
		<!-- This section it for import the header and Nav-->
		<section id="header">
			<?php get_header(); ?>
			
		</section>
		<!-- This section it for import the content frontpage or entries if you use a sidebar  upload into this -->
		<section id="content">	
			<?php $args = array(
				'posts_per_page'   => 5,
				'offset'           => 0,
				'category'         => '',
				'orderby'          => 'post_date',
				'order'            => 'DESC',
				'include'          => '',
				'exclude'          => '',
				'meta_key'         => '',
				'meta_value'       => '',
				'post_type'        => 'post',
				'post_mime_type'   => '',
				'post_parent'      => '',
				'post_status'      => 'publish',
				'suppress_filters' => true ); 
				$posts_array = get_posts( $args ); 
				echo "<ul><li>Menu";
	   			echo '<div>';
	   			echo '<ul>';
	   			foreach ($posts_array as $val) {
					echo '<li>';
					echo '<a href=../';
					echo $val -> post_title;
					echo '>';
					echo $val -> post_title;
					echo '</a>';
					echo '</li>';
				}
				echo '</ul>';
				echo '</div>';
				echo "</li></ul>";
				?>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				 <div class="entry">
  				 <?php
  				 the_content(); ?>
 				</div>
				<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
				<?php endif; ?>
		</section>
		
		<!-- This section it for import the footer-->
		<section id="footer">
			<?php get_footer(); ?>
		</section>
	</section>
</body>
</html>